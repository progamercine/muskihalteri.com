export class Proizvod {
    constructor(e, o, n, a = 0, i, t) {
        (this.model = e), (this.cena = o), (this.slikaPNG = n), (this.kolicina = a), (this.url = i), (this.slikaWebP = t), console.log("ispis iz konstruktora", a);
    }

    static kupi(e, o, n = 1, a) {
        localforage
            .getItem(e, function (i, t) {
                return (
                    console.log("ispis modela u kupi", o.model),
                        console.log("kolicina korpa", a),
                        null !== t
                            ? (console.log("ispis modela if", t.model), null !== t.model && ((o.kolicina = n + t.kolicina), console.log("nije null"), console.log(e), void 0 !== a && (o.kolicina = a)))
                            : (console.log("JESTE null"), (o.kolicina += n)),
                        localforage.setItem(e, o)
                );
            })
            .then(function () {
                localforage.getItem(e, function (e, o) {
                    korpaKupi();
                });
            });
    }

    static obrisi(e) {
        localforage
            .removeItem(e)
            .then(function () {
                console.log("unos", e, "obrisan");
            })
            .then(function () {
                localforage.getItem(e, function (e, o) {
                    korpaKupi();
                });
            });
    }
}

let proizvodA = new Proizvod("A", 1500, "images/products/model-a-resize.png", 0, "naruci-proizvod/tregeri-za-kosulju-i-carape.html", "images/products/model-a-resize.webp"),
    proizvodB = new Proizvod("B", 1500, "images/products/model-b-resize.png", 0, "naruci-proizvod/muski-halteri-za-kosulje.html", "images/products/model-b-resize.webp"),
    proizvodC = new Proizvod("C", 1500, "images/products/model-c-resize.png", 0, "naruci-proizvod/tregeri-za-kosulju.html", "images/products/model-c-resize.webp"),
    proizvodD = new Proizvod("D", 1500, "images/products/model-d-resize.png", 0, "naruci-proizvod/drzaci-za-carape.html", "images/products/model-d-resize.webp"),
    proizvodE = new Proizvod("E", 1500, "images/products/model-e-resize.png", 0, "naruci-proizvod/drzaci-za-kosulju.html", "images/products/model-e-resize.webp");
var nizProizvodaIndex = [proizvodA, proizvodB, proizvodC, proizvodD, proizvodE],
    proizvodiHTMLIndex = document.querySelector(".proizvodi"),
    oglasiIndex = "";
for (let e = 0; e < nizProizvodaIndex.length; e++) {
    var offset = "";
    0 === e && (offset = "offset-lg-1"),
        (oglasiIndex +=
            '<div class="col-sm-12 col-md-6 col-lg-2  ' +
            offset +
            ' d-flex">\n                <div class="product d-flex flex-column">\n                    <a class="img-prod" href="' +
            nizProizvodaIndex[e].url +
            '">\n<picture>\n                        <source srcset="' +
            nizProizvodaIndex[e].slikaWebP +
            '" type="image/webp">\n                        <img alt="Colorlib Template"\n                             class="img-fluid"\n                             src="' +
            nizProizvodaIndex[e].slikaPNG +
            '">\n                    </picture>\n                        <div class="overlay"></div>\n                    </a>\n                    <div class="text py-3 pb-4 px-3">\n                        <div class="d-flex">\n                            <div class="cat">\n                                <span>Utisci</span>\n                            </div>\n                            <div class="rating">\n                                <p class="text-right mb-0">\n                                    <span class="ion-ios-star"></span>\n                                    <span class="ion-ios-star"></span>\n                                    <span class="ion-ios-star"></span>\n                                    <span class="ion-ios-star"></span>\n                                    <span class="ion-ios-star"></span>\n                                </p>\n                            </div>\n                        </div>\n                        <h3>\n                            <a href="javascript:void(0)">Model ' +
            nizProizvodaIndex[e].model +
            '</a>\n                        </h3>\n                        <div class="pricing">\n                            <p class="price"><span>' +
            nizProizvodaIndex[e].cena +
            ' din.</span></p>\n                        </div>\n                        <p class="bottom-area d-flex px-3">\n                            <a class="pointer add-to-cart text-center py-2 mr-1"\n                               "><span>Dodaj u\n                                                                                                          korpu<i\n                                     class="ion-ios-add ml-1"></i></span></a>\n                            <a class="buy-now text-center py-2" href="korpa.html">Kupi<span><i\n                                 class="ion-ios-cart ml-1"></i></span>\n                            </a>\n                        </p>\n                    </div>\n                </div>\n            </div>');
}

function eventListenerTrigger() {
    let e,
        o = document.querySelectorAll(".add-to-cart");
    for (let e = 0; e < o.length; e++)
        o[e].addEventListener("click", function () {
            Proizvod.kupi(nizProizvodaIndex[e].model, nizProizvodaIndex[e]);
        });
    let n = document.querySelector("#modelPoruci"),
        a = document.querySelector("#modelNaslov");
    if ((a && ((a = a.innerHTML), (e = a.charAt(7)), console.log(e)), n))
        for (let o = 0; o < nizProizvodaIndex.length; o++)
            n.addEventListener("click", function () {
                let n = parseInt(document.querySelector("#quantity").value);
                nizProizvodaIndex[o].model === e && (Proizvod.kupi(nizProizvodaIndex[o].model, nizProizvodaIndex[o], n), console.log(n));
            });
}

proizvodiHTMLIndex && (proizvodiHTMLIndex.innerHTML = oglasiIndex), eventListenerTrigger();
let kolicinaKorpaNavigacija = document.querySelector("#korpa-kolicina");
var korpaKupi = () => {
    let e = 0,
        o = document.querySelector("#korpaTbody");
    o && (o.innerHTML = ""),
        console.log("ispis iz kupi"),
        localforage
            .iterate(function (e, o, n) {
                console.log("USAO SAM U promise ispis"), ispisProizvodaKorpa(o, e);
            })
            .then(function () {
                console.log("radim i posle"), ukupnaCenaKorpa();
            })
            .then(function () {
                let e = [
                    document.querySelectorAll("#korpaKolicinaA"),
                    document.querySelectorAll("#korpaKolicinaB"),
                    document.querySelectorAll("#korpaKolicinaC"),
                    document.querySelectorAll("#korpaKolicinaD"),
                    document.querySelectorAll("#korpaKolicinaE"),
                ];
                for (let o = 0; o < e.length; o++)
                    if (e[o][0]) {
                        let n;
                        e[o][0].addEventListener("keyup", function (a) {
                            console.log(a),
                                console.log(a.target.value, "lista kolicina korpas"),
                                (n = parseInt(e[o][0].value)),
                            isNaN(n) && ((n = 0), console.log("usao sam u NANA")),
                                console.log(typeof n, "kolicina poruci type"),
                                console.log(n, "kolicina poruci"),
                                document.getElementById("korpaTbody"),
                                Proizvod.kupi(nizProizvodaIndex[o].model, nizProizvodaIndex[o], n, n),
                                fadeOutEffect();
                        });
                    }
                console.log("Iteration has completed");
            })
            .then(function () {
                (korpaCenaSviModeliZajedno = 0), korpaObrisi();
            })
            .then(
                localforage
                    .iterate(function (o, n, a) {
                        e += o.kolicina;
                    })
                    .then(function () {
                        (kolicinaKorpaNavigacija.innerText = e), korpaNavAnim();
                    })
            );
};
let korpaCenaUkupno,
    drzacZaKosulju = "Držači za košulju",
    drzacZaCarape = "Držači za čarape",
    drzacZaKosuljuICarape = "Držači za košulju i čarape",
    korpaCenaSviModeliZajedno = 0;
var ispisProizvodaKorpa = (e, o) => {
    let n,
        a = document.querySelector("#korpaCenaSviModeliZajedno");
    "A" === e && (n = drzacZaKosuljuICarape),
    ("B" !== e && "C" !== e && "E" !== e) || (n = drzacZaKosulju),
    "D" === e && (n = drzacZaCarape),
        (korpaCenaUkupno = o.kolicina * o.cena + " din."),
        (korpaCenaSviModeliZajedno += o.kolicina * o.cena),
        console.log(korpaCenaSviModeliZajedno, "Korpa ukupna cena TEST"),
    a && (a.innerHTML = korpaCenaSviModeliZajedno + " din");
    let i = document.querySelector("#korpaTbody");
    i &&
    (i.innerHTML +=
        '<tr class="text-center" id="model' +
        e +
        'KorpaToggle">\n                            <td class="product-remove">\n                                <a id="korpaUkupno' +
        e +
        '"><span class="ion-ios-close"></span></a>\n                            </td>\n                            <td class="image-prod">\n                                <div class="img" style="background-image:url(' +
        o.slikaPNG +
        '); "></div>\n                            </td>\n                            <td class="product-name">\n                                <h3>Muški halteri Model "' +
        e +
        '"</h3>\n                                <p>' +
        n +
        '</p>\n                            </td>\n                            <td class="price" id="cena' +
        e +
        '">' +
        o.cena +
        ' din.</td>\n                            <td class="quantity">\n                                <div class="input-group mb-3">\n                                    <input class="quantity form-control input-number" id="korpaKolicina' +
        e +
        '" max="100"\n                                           min="1"\n                                           name="quantity" type="text" value="' +
        o.kolicina +
        '">\n                                </div>\n                            </td>\n                            <td class="total" id="korpaUkupno' +
        e +
        '">' +
        korpaCenaUkupno +
        "</td>\n                        </tr>");
};
let ukupnaCenaKorpa = () => {
    let praznaKorpaAlert = document.querySelector('#praznaKorpaAlert');
    let korpaBtn = document.querySelector('#korpaBtn');
    localforage.length().then(function (e) {
        if (0 === e) {
            praznaKorpaAlert.style.display = 'block';
            korpaBtn.classList.add('disabled');
            let e = document.querySelector("#korpaCenaSviModeliZajedno");
            (korpaCenaSviModeliZajedno = 0), e && (e.innerHTML = korpaCenaSviModeliZajedno + " din");
        } else {
            korpaBtn.classList.remove('disabled');
            praznaKorpaAlert.style.display = 'none';

        }
        console.log(e);
    });
};
var iteracijaPorudzbina = () => {
    localforage
        .iterate(function (e, o, n) {
            ispisProizvodaPorudzbina(o, e);
        })
        .then(function () {
            cenaDostaveRacunanje();
        });
};
let porudzbinaCenaUkupno,
    porudzbinaCenaSviModeliZajedno = 0;
var ispisProizvodaPorudzbina = (e, o) => {
    let n = document.querySelector("#proizvodiUKorpi");
    (porudzbinaCenaUkupno = o.kolicina * o.cena + " din."),
        (porudzbinaCenaSviModeliZajedno += o.kolicina * o.cena),
    0 !== o.kolicina &&
    null !== n &&
    (n.innerHTML +=
        '<p class="d-flex">\n                                        <span class="font-weight-bold" >Muški halteri model "' +
        e +
        '"</span> <input hidden name="model' +
        e +
        '" value="' +
        o.kolicina +
        '"/><span class="text-center">x</span><span\n                                         class="text-center font-weight-bold">' +
        o.kolicina +
        '\n                                </span><span class="text-center">' +
        porudzbinaCenaUkupno +
        "\n                                                                                                           </span>\n                                    </p><hr>");
};
let proizvodUkupneCene;
iteracijaPorudzbina();
let ukupnoZaPlacanjePorudzbina = (e) => {
        let o = document.querySelector("#cenaTregeraPlacanje"),
            n = document.querySelector("#cenaDostave"),
            a = document.querySelector("#ukupnoZaPlacanjePorudzbina");
        (proizvodUkupneCene = parseInt(porudzbinaCenaSviModeliZajedno) + e), null !== o && ((o.innerText = porudzbinaCenaSviModeliZajedno + " din."), (n.innerText = e + " din."), (a.innerText = proizvodUkupneCene + " din."));
    },
    cenaDostave = 0,
    cenaDostaveRacunanje = () => {
        null !== pouzecemRadio && null !== prekoRacunaRadio && (pouzecemRadio.checked ? (cenaDostave = 400) : prekoRacunaRadio.checked && (cenaDostave = 380)), ukupnoZaPlacanjePorudzbina(cenaDostave);
    },
    pouzecemRadio = document.querySelector("#pouzecem"),
    prekoRacunaRadio = document.querySelector("#prekoRacuna"),
    listaRadioPlacanje = [pouzecemRadio, prekoRacunaRadio];
for (let e = 0; e < listaRadioPlacanje.length; e++)
    null !== listaRadioPlacanje[e] &&
    listaRadioPlacanje[e].addEventListener("click", function () {
        cenaDostaveRacunanje();
    });
let korpaObrisi = () => {
    let e = [document.querySelector("#korpaUkupnoA"), document.querySelector("#korpaUkupnoB"), document.querySelector("#korpaUkupnoC"), document.querySelector("#korpaUkupnoD"), document.querySelector("#korpaUkupnoE")];
    for (let o = 0; o < e.length; o++)
        e[o] &&
        e[o].addEventListener("click", function () {
            console.log("radi klik na brisanje  "), Proizvod.obrisi(nizProizvodaIndex[o].model);
        });
};

function fadeOutEffect() {
    var e = document.querySelector(".loading");
    e.style.display = "block";
    var o = setInterval(function () {
        e.style.opacity || (e.style.opacity = 1), e.style.opacity > 0 ? (e.style.opacity -= 0.1) : (clearInterval(o), (e.style.display = "none"), (e.style.opacity = 1));
    }, 50);
}

korpaKupi();
let korpaNavElem = document.querySelector("#korpaKolicina"),
    korpaNavAnim = () => {
        korpaNavElem.classList.toggle("jello-horizontal"), korpaNavElem.classList.toggle("jello-horizontal2");
    },
    drustveneMreze = document.querySelectorAll(".drustvene-mreze");
for (let e = 0; e < drustveneMreze.length; e++)
    drustveneMreze[e].addEventListener("mouseover", function () {
        drustveneMreze[e].classList.add("text-pop-up-top"), drustveneMreze[e].classList.remove("text-pop-up-top2"), drustveneMreze[e].classList.remove("ftco-animate");
    });
for (let e = 0; e < drustveneMreze.length; e++)
    drustveneMreze[e].addEventListener("mouseout", function () {
        drustveneMreze[e].classList.add("text-pop-up-top2"), drustveneMreze[e].classList.remove("text-pop-up-top"), drustveneMreze[e].classList.remove("ftco-animate");
    });
$(function () {
    $(".btn-6, .btn-6-bela")
        .on("mouseenter", function (e) {
            var o = $(this).offset(),
                n = e.pageX - o.left,
                a = e.pageY - o.top;
            $(this).find("span").css({top: a, left: n});
        })
        .on("mouseout", function (e) {
            var o = $(this).offset(),
                n = e.pageX - o.left,
                a = e.pageY - o.top;
            $(this).find("span").css({top: a, left: n});
        }),
        $("[href=#]").click(function () {
            return !1;
        });
});
let brojaMenuMobilni = 0,
    dropdownTrigger = document.getElementById("dropdown04"),
    hamburgerElement = document.querySelectorAll(".navbar-toggler");
for (let e = 0; e < hamburgerElement.length; e++)
    hamburgerElement[e].addEventListener("click", function () {
        brojaMenuMobilni = 0;
    });
dropdownTrigger.addEventListener("click", function () {
    0 === brojaMenuMobilni && (dropdownTrigger.click(), brojaMenuMobilni++);
});
let poruciBtn = document.querySelector("#poruciDugme"),
    modalModeli = document.querySelector("#modelPoruceniModeli"),
    ukupnaCenaModal = document.querySelector("#ukupnaCenaModal");
poruciBtn &&
poruciBtn.addEventListener("click", function () {
    localforage
        .iterate(function (e, o, n) {
            (modalModeli.innerHTML +=
                '<p class="d-flex">\n                                        <span class="font-weight-bold" >Muški halteri model "' +
                e.model +
                '"</span> <input hidden name="model' +
                e.model +
                '" value="' +
                e.kolicina +
                '"/><span class="text-center">&nbspx&nbsp</span><span\n                                         class="text-center font-weight-bold">' +
                e.kolicina +
                "\n                                </span>                                    </p>"),
                (ukupnaCenaModal.innerHTML = proizvodUkupneCene + " din."),
                console.log("modal radim");
        })
        .then(function () {
            document.querySelector(".md-close").addEventListener("click", function () {
                console.log("dugme zatvori"), localforage.clear(), (window.location.href = "../index.html");
            });
        });
});
let modalZatvori = document.querySelector(".md-close");
modalZatvori &&
modalZatvori.addEventListener("click", function () {
    window.location.href = "index.html";
}),
    $(function () {
        $("#kontaktForma, #porudzbinaForma").on("submit", function () {
            $(".md-modal").addClass("md-show"), console.log("submit u kontaktu radi");
        }),
            $(".md-close").on("click", function () {
                setTimeout(function () {
                    $(".md-modal").removeClass("md-show");
                }, 1e3);
            });
    });
var lazyLoadInstance = new LazyLoad({elements_selector: ".lazy"});
let addToCart = document.querySelectorAll(".add-to-cart");
for (let e = 0; e < addToCart.length; e++) addToCart[e].addEventListener("click", pokretanjeToasta);

function pokretanjeToasta() {
    var e = document.getElementById("snackbar");
    (e.className = "show"),
        setTimeout(function () {
            e.className = "";
        }, 2e3);
}

//
// // konstruktor za kreiranje objekta sa brojem telefona i porukom
// function SmsObj(telefon, textPoruke) {
//     {
//         this.toNumber = telefon;
//         this.textMessage = textPoruke;
//     }
// }
//
//
// const porudzbinaForma = document.querySelector('#porudzbinaForma');
// console.log('porudzbina forma ', porudzbinaForma);
//
// // Listener za slanje SMS-a prodavcu i kupcu nakon porudzbine
// porudzbinaForma.addEventListener('submit', function () {
//
//
//     // Broj telefona kupca iz forme prilikom porudzbine
//     const brojTelefonaKupca = document.querySelector('#telefon').value;
//
//     // Broj telefona prodavca
//     const brojTelefonaProdavca = '+381677020272';
//
//     // Genericka SMS poruka za kupca prilikom narucivanja proizvoda
//     const porukaKupac = `
//
// muskihalteri.com
// `;
//
// // Genericka SMS poruka za prodavca prilikom porucivanja proizvoda
//     const porukaProdavac = `Porudžbina na muskihalteri.com. Proveri mail na office@muskihalteri.com`;
//
//     // Objekat sms kupac
//     let kupacObjSMS = new SmsObj(brojTelefonaKupca, porukaKupac);
//
//     // Konvertovanje kupac objekta u JSON string
//     let kupacJson = JSON.stringify(kupacObjSMS);
//
//     // Obj za sms prodavac
//     let prodavacObjSms = new SmsObj(brojTelefonaProdavca, porukaProdavac);
//
//     // Konvertovanje obj prodavac u JSON string
//     let prodavacJson = JSON.stringify(prodavacObjSms);
//
//
//     console.log(prodavacJson);
//     console.log(kupacJson);
//
//     const xhr = new XMLHttpRequest();
//     const url = 'https://159.69.127.82:8443/send-sms1';
//
//     xhr.open("POST", url);
//     xhr.setRequestHeader('Content-Type', 'application/json; utf-8');
//     xhr.send(prodavacJson);
//
// });
//
function SmsObj(brojTelefona, poruka) {
    this.phoneNumber = brojTelefona;
    this.message = poruka
}

const porudzbinaForma = document.querySelector('#porudzbinaForma');


let poruceniModeliLista = [];
async function kreiranjeListePorucenihModela() {
 await localforage.iterate(function(value, key, iterationNumber) {
        console.log(value, key, iterationNumber);
        poruceniModeliLista.push(`Model: ${value.model} - količina: ${value.kolicina}`);
    });
}

function kreiranjeSmsPorukeZaKupca(listaModela) {
    let poruka = "Vasa porudzbina na sajtu muskihalteri.com je uspesno primljena. Porucili ste:\n";
    for(const model of listaModela) {
        poruka += "\n" + model;
    }
    poruka += "\n\nPaket ce Vam biti dostavljen u roku od 1-2 radna dana.\n\nHvala na poverenju,\nmuskihalteri.com";
    return poruka;
}

function kreiranjeSmsPorukeZaProdavca(listaModela) {
    let poruka = "Porudzbina:\n";
    for(const model of listaModela) {
        poruka += `\n${model}`;
    }
    return poruka;
}


porudzbinaForma.addEventListener('submit',async() => {
  await kreiranjeListePorucenihModela();
  const porukaKupac = kreiranjeSmsPorukeZaKupca(poruceniModeliLista);
  const brojTelefonaKupca = document.querySelector('#telefon').value;
    console.log("Poruka za kupca: ", porukaKupac);
    const porukaProdavac = kreiranjeSmsPorukeZaProdavca(poruceniModeliLista);
    const brojTelefonaProdavca = '+381642226830';
    const dataKupac = new SmsObj(brojTelefonaKupca, porukaKupac);
    const dataProdavac = new SmsObj(brojTelefonaProdavca, porukaProdavac);


    const slanjePorukeKupac = () => {
        return fetch('https://programercine.online:4000/api/v1/sms', {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(dataKupac),
        })

    };

    const slanjePorukeProdavac = () => {
        return fetch('https://programercine.online:4000/api/v1/sms', {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(dataProdavac),
        })

    };

    slanjePorukeKupac();
    slanjePorukeProdavac();

});

